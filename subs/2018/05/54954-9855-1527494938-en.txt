webvtt

1
00:00:00.398 --> 00:00:03.914
Great news! Our couch problem is solved!

2
00:00:03.920 --> 00:00:05.210
What couch problem?

3
00:00:06.676 --> 00:00:08.506
Uh. the one that's got
your knees up higher

4
00:00:08.516 --> 00:00:09.997
than when you're getting a pap smear.

5
00:00:11.862 --> 00:00:15.901
So. Logan's remodeling her condo.
and she's giving me her old couch!

6
00:00:15.907 --> 00:00:17.464
Oh. my God. oh. my God. oh. my God...

7
00:00:18.066 --> 00:00:20.067
- Help me up so I can be excited.
- Okay.

8
00:00:21.171 --> 00:00:23.505
We're getting a new couch!

9
00:00:23.511 --> 00:00:25.896
- And the best part is...
- How can there be better parts?

10
00:00:25.902 --> 00:00:29.209
It's a-comin a-right a-now!

11
00:00:29.710 --> 00:00:32.607
- Oh. my God. do you love it?
- Oh. my God. I hate it!

12
00:00:33.490 --> 00:00:35.747
What? Why?

13
00:00:36.110 --> 00:00:38.077
It's hideous.

14
00:00:38.388 --> 00:00:42.292
It looks like the product
of years of couch incest.

15
00:00:42.950 --> 00:00:45.370
It's a-goin' a-back a-now!

16
00:00:45.739 --> 00:00:46.966
It's not going anywhere.

17
00:00:46.972 --> 00:00:48.628
Do you understand that Logan is my boss?

18
00:00:48.634 --> 00:00:50.537
What if she comes over
and the couch isn't here?

19
00:00:50.543 --> 00:00:53.233
What if anyone else comes
over and the couch is here?

20
00:00:54.440 --> 00:00:57.518
Gabi... Logan picked me out
of everyone in the office

21
00:00:57.524 --> 00:01:00.548
to give the couch to. Sending it
back would be a slap in her face.

22
00:01:00.950 --> 00:01:03.122
But I don't want it!

23
00:01:03.664 --> 00:01:04.864
Well.

24
00:01:05.250 --> 00:01:07.557
I mean. I don't wanna be
an assistant my entire life.

25
00:01:07.563 --> 00:01:09.642
And when it comes time for a promotion.

26
00:01:09.648 --> 00:01:13.047
who do you think'll get it? The
girl who rudely gave back the couch.

27
00:01:13.053 --> 00:01:14.438
or the girl who kept the couch.

28
00:01:14.444 --> 00:01:17.552
because it reflects on her
boss' kindness and good taste?

29
00:01:17.558 --> 00:01:22.248
So you really think that... this couch

30
00:01:22.254 --> 00:01:24.315
is. uh... gonna get you a promotion?

31
00:01:24.321 --> 00:01:26.528
'Cause you know. that's
really not how it works. Sofia.

32
00:01:27.100 --> 00:01:29.923
Oh. Gabi. that's exactly how it works.

33
00:01:30.282 --> 00:01:32.447
Before this. Logan and
I had nothing in common.

34
00:01:32.453 --> 00:01:35.239
Now we have the couch.
So. whenever we're alone.

35
00:01:35.245 --> 00:01:38.674
or in the bathroom. or there's a
lull in conversation. she'll be like.

36
00:01:38.680 --> 00:01:40.088
"How's the couch?"

37
00:01:40.247 --> 00:01:43.377
And I'll be like. "Oh.
my God! We love it!"

38
00:01:43.383 --> 00:01:46.386
And she'll be like. "I knew you
would. You want a promotion?"

39
00:01:47.254 --> 00:01:50.496
And that. my friend. is how it works.

40
00:01:51.074 --> 00:01:54.315
Okay. if it's that important to
you. I guess we'll keep the couch.

41
00:01:54.321 --> 00:01:57.279
- but just know that I hate it!
- Thank you!

42
00:01:57.611 --> 00:02:00.628
And one more thing. and you might
not be interested in this. but. um...

43
00:02:00.634 --> 00:02:02.446
there is a matching ottoman.

44
00:02:27.595 --> 00:02:30.112
Okay... what's goin' on in here?

45
00:02:30.118 --> 00:02:31.967
I'm makin' freakin' fudge.

46
00:02:31.973 --> 00:02:34.770
Well. why make fudge when you
could have this white chocolate?

47
00:02:35.681 --> 00:02:37.070
Ugh!

48
00:02:38.187 --> 00:02:40.191
Come on. we haven't used
the punch card in a week.

49
00:02:40.197 --> 00:02:42.758
Which means I... had to punch myself.

50
00:02:43.409 --> 00:02:45.311
All right. here's something that'll...

51
00:02:45.317 --> 00:02:47.384
keep you from thinkin' about sex.

52
00:02:47.698 --> 00:02:49.894
Ugh. What is that?

53
00:02:50.104 --> 00:02:51.511
That's a couch.

54
00:02:51.694 --> 00:02:53.520
Digesting another couch?

55
00:02:54.301 --> 00:02:57.114
Sofia's boss dumped
her crappy couch on us.

56
00:02:57.120 --> 00:02:58.481
Oh! Well. you're in luck.

57
00:02:58.487 --> 00:03:00.568
I just developed this new
app for called "Crap4Cash."

58
00:03:00.574 --> 00:03:01.809
Hey... that's great.

59
00:03:01.815 --> 00:03:04.623
I have a ton of crap
and I need a ton of cash.

60
00:03:05.719 --> 00:03:07.342
Everyone does. Gabi.

61
00:03:07.348 --> 00:03:08.921
But what does everybody hate?

62
00:03:08.927 --> 00:03:10.812
Meeting the creep who bought your crap.

63
00:03:10.818 --> 00:03:12.999
That's why I developed an
anonymous delivery service

64
00:03:13.005 --> 00:03:15.538
that shows up so you don't
have to meet the creep...

65
00:03:15.544 --> 00:03:17.019
who bought your crap.

66
00:03:17.221 --> 00:03:19.902
- Oh. my God! That's genius!
- Give me your phone.

67
00:03:19.908 --> 00:03:22.640
- I'll put your couch on there.
- No... I wish. but I can't sell it.

68
00:03:22.646 --> 00:03:24.074
Well. you don't have to accept any bids.

69
00:03:24.080 --> 00:03:25.876
I'll just show you how it
works. give me your phone.

70
00:03:26.674 --> 00:03:28.791
Oh... fudge!

71
00:03:29.376 --> 00:03:32.081
Wait. wait. wait. wait. wait.
wait... What are you doing?

72
00:03:32.087 --> 00:03:34.533
Today is our first
weigh-in at Fat Fighters.

73
00:03:35.134 --> 00:03:36.931
Yeah. well. we're not there yet.

74
00:03:37.446 --> 00:03:39.850
So. let me get this straight.
you're taking the thing

75
00:03:39.856 --> 00:03:42.218
with the most fat in
it to a diet center?

76
00:03:43.091 --> 00:03:45.318
No. I'm also taking Yolanda.

77
00:03:51.496 --> 00:03:53.395
- What was that?
- That's the sound the app makes

78
00:03:53.401 --> 00:03:54.793
when someone bids on your crap.

79
00:03:54.799 --> 00:03:56.434
Someone just offered you 500.

80
00:03:57.294 --> 00:03:58.723
Dollars?

81
00:04:00.841 --> 00:04:02.631
Oh. my God. that's with the photo?

82
00:04:03.035 --> 00:04:04.335
650!

83
00:04:04.341 --> 00:04:06.927
My God. are you kidding
me? No. no. I can't sell it.

84
00:04:06.933 --> 00:04:08.396
I promised Sofia I wouldn't.

85
00:04:08.568 --> 00:04:10.943
- Eight hund-oh!
- Sold!

86
00:04:16.262 --> 00:04:19.279
Oh. my God... that's the
rabbi from your wedding!

87
00:04:19.285 --> 00:04:21.109
Oh. Rabbi Shapiro.

88
00:04:21.115 --> 00:04:24.061
- Let's go say hi.
- Oh... Hell. no!

89
00:04:24.067 --> 00:04:25.875
Uh... I never told you this...

90
00:04:25.881 --> 00:04:27.764
but we hooked up on your wedding night.

91
00:04:28.373 --> 00:04:29.980
You slept with him?

92
00:04:30.371 --> 00:04:32.672
Haven't the Jews suffered enough?

93
00:04:34.099 --> 00:04:37.063
Shut up! We were dating for a while.

94
00:04:37.069 --> 00:04:38.470
then suddenly he stopped calling.

95
00:04:38.476 --> 00:04:40.036
I have no idea what happened.

96
00:04:40.042 --> 00:04:42.371
I do. You let him see you naked.

97
00:04:42.842 --> 00:04:44.631
Right. I'm outta here.

98
00:04:44.637 --> 00:04:49.217
Oh. really? So you have no
desire to rekindle with the rabbi?

99
00:04:49.525 --> 00:04:51.336
You're damn right I don't.

100
00:04:52.025 --> 00:04:55.294
Although he does look
cute in that rabbi sweater.

101
00:04:56.235 --> 00:04:59.612
Hello. everybody. I am Ben Shapiro.

102
00:04:59.618 --> 00:05:02.637
a rabbi and a Fat Fighter counselor.

103
00:05:02.643 --> 00:05:06.323
I went from wandering around the
desert to wondering. "Where's dessert?

104
00:05:07.213 --> 00:05:09.181
He still makes me laugh.

105
00:05:09.187 --> 00:05:12.280
- Today's topic... emotions.
- _

106
00:05:12.819 --> 00:05:15.294
Instead of eating our feelings.

107
00:05:15.300 --> 00:05:18.901
we have to learn to
express it. Let it out!

108
00:05:18.907 --> 00:05:21.120
Why the hell you stop calling me?

109
00:05:21.525 --> 00:05:23.098
Yolanda?

110
00:05:24.425 --> 00:05:25.748
How are you?

111
00:05:28.851 --> 00:05:31.971
Hey!? Hey! You can't eat that!

112
00:05:32.054 --> 00:05:33.539
Fatty says what?

113
00:05:35.046 --> 00:05:37.142
That's like 20 points!

114
00:05:37.148 --> 00:05:40.654
Oh! This is actually fat-free fudge!

115
00:05:40.660 --> 00:05:41.909
Want some. honey?

116
00:05:44.380 --> 00:05:46.137
Oh. my God!

117
00:05:46.143 --> 00:05:47.706
Do you know how long I've been looking

118
00:05:47.712 --> 00:05:50.721
for a fat-free dessert
that actually tastes good?

119
00:05:50.727 --> 00:05:54.200
Judging from your Cosby
sweater. I'd say since the '90s.

120
00:05:56.679 --> 00:05:59.094
Where did get this? I
would pay a fortune for it.

121
00:05:59.100 --> 00:06:02.360
Well. you would have to. Because
it's very. very hard to find.

122
00:06:02.366 --> 00:06:04.414
Name your price. my husband's rich.

123
00:06:04.420 --> 00:06:06.109
You're married?

124
00:06:07.390 --> 00:06:10.196
I had to break it off
because there was no future.

125
00:06:10.202 --> 00:06:14.379
Being with a non-Jewish woman
would break my mother's heart.

126
00:06:15.356 --> 00:06:17.374
But you know what? And
I don't mind saying this

127
00:06:17.380 --> 00:06:18.866
in front of the entire group.

128
00:06:18.872 --> 00:06:21.256
After seeing you again. and feeling.

129
00:06:21.262 --> 00:06:22.663
the way I feel right now.

130
00:06:22.669 --> 00:06:25.452
I am reminded of those
very poignant words.

131
00:06:25.458 --> 00:06:28.834
you must not let anyone
define your limits

132
00:06:28.840 --> 00:06:31.058
because of where you come from.

133
00:06:31.064 --> 00:06:34.469
Your only limit... is your soul.

134
00:06:34.475 --> 00:06:36.887
- Was that Gandhi?
- Ratatouille.

135
00:06:41.573 --> 00:06:43.353
Ooh. Sofia!

136
00:06:43.749 --> 00:06:45.650
- I have a huge surprise.
- Me. too!

137
00:06:45.656 --> 00:06:48.192
Okay. mine's more of a
visual. so I'll go first.

138
00:06:48.198 --> 00:06:50.079
Logan's coming over for
dinner to see the couch!

139
00:06:53.165 --> 00:06:55.165
- What?
- Yeah.

140
00:06:55.171 --> 00:06:57.478
So get this. we were in the
bathroom. and for the first time ever.

141
00:06:57.484 --> 00:06:58.974
there were no uncomfortable silences.

142
00:06:58.980 --> 00:07:00.960
Why? Because we talked about the couch!

143
00:07:00.966 --> 00:07:02.864
And I was like. "Do you wanna
come over and see the couch?"

144
00:07:02.870 --> 00:07:05.281
And she was all. "Sure. I'd love to."

145
00:07:05.287 --> 00:07:08.356
And... so now. she's coming over
for dinner with me and the couch.

146
00:07:08.362 --> 00:07:10.062
which means the promotion
can't be far off.

147
00:07:10.068 --> 00:07:11.368
What's your surprise?

148
00:07:13.561 --> 00:07:15.083
Gabi. where's the couch?

149
00:07:15.763 --> 00:07:17.803
So. surprise.

150
00:07:20.468 --> 00:07:23.135
Uh... where's the couch?

151
00:07:23.141 --> 00:07:26.006
Um... I turned it into $800.

152
00:07:26.527 --> 00:07:29.190
- You sold the couch?
- I didn't mean to.

153
00:07:29.196 --> 00:07:31.110
Well. get it back!

154
00:07:32.385 --> 00:07:34.893
That might not be exactly possible.

155
00:07:34.899 --> 00:07:37.890
because I sold it on
Josh's new app. "Crap4Cash."

156
00:07:37.896 --> 00:07:39.588
where everything you sell is anonymous.

157
00:07:39.594 --> 00:07:41.104
So you never have to meet the creep

158
00:07:41.110 --> 00:07:43.172
who bought your crap!

159
00:07:43.986 --> 00:07:47.390
I cannot believe you. I told
you that this was my ticket in!

160
00:07:48.455 --> 00:07:49.696
Into what?

161
00:07:50.383 --> 00:07:52.693
Into... a promotion!

162
00:07:52.699 --> 00:07:54.741
Do you listen to anything I say?

163
00:07:54.832 --> 00:07:57.200
Like... do you even know
that I wanna be a writer?

164
00:07:57.701 --> 00:07:59.208
I do now.

165
00:07:59.214 --> 00:08:02.469
Oh. my God! How am I
even friends with you?

166
00:08:02.475 --> 00:08:04.776
- Why am I even friends with you?
- I don't understand why you're getting

167
00:08:04.782 --> 00:08:07.676
- so mad. I do bad stuff all the time!
- This isn't... it's...

168
00:08:07.682 --> 00:08:10.860
this isn't like any other time.
Gabi. this is about my career!

169
00:08:10.866 --> 00:08:12.079
As a writer!

170
00:08:12.085 --> 00:08:14.766
Gabi. I told you how important
this was to me and... and...

171
00:08:14.772 --> 00:08:16.945
now I may never get a
promotion. and not only that.

172
00:08:16.951 --> 00:08:18.662
come Monday. I might
not even have a job.

173
00:08:18.668 --> 00:08:20.654
- Sofia. I'm really sorry.
- Oh. my God. Gabi.

174
00:08:20.660 --> 00:08:23.074
you are always sorry.
You're always sorry.

175
00:08:23.080 --> 00:08:25.083
How many more times
are you gonna be sorry?

176
00:08:25.797 --> 00:08:27.003
One more.

177
00:08:28.379 --> 00:08:29.793
That was rhetorical.

178
00:08:29.884 --> 00:08:33.512
Rhetorical. Hey. that's
a good word for a writer.

179
00:08:34.119 --> 00:08:35.420
I'm done.

180
00:08:38.313 --> 00:08:40.118
We can still get the ottoman!

181
00:08:44.178 --> 00:08:45.584
- Ohhh... you know.
- Ohhh...

182
00:08:45.590 --> 00:08:49.061
I never thought I'd be on a
date at a Fat Fighters clinic!

183
00:08:49.067 --> 00:08:51.453
Well. I was gonna take
you to a nice restaurant.

184
00:08:51.459 --> 00:08:54.022
but I know how difficult
it is to dine out

185
00:08:54.028 --> 00:08:55.520
when you start a new program.

186
00:08:55.526 --> 00:08:57.019
Ohhh... and who knew

187
00:08:57.025 --> 00:08:59.694
you were such an amazing cook.

188
00:08:59.700 --> 00:09:02.105
I mean... that five-point stuffing

189
00:09:02.111 --> 00:09:04.197
was mother-stuffing delicious!

190
00:09:06.125 --> 00:09:07.600
Thank you.

191
00:09:07.606 --> 00:09:09.736
So how'd you end up workin' here anyway?

192
00:09:09.742 --> 00:09:12.691
Well. about a month ago. I
was napping on my mom's couch.

193
00:09:12.697 --> 00:09:15.740
and it sorta kinda
broke under my weight.

194
00:09:15.746 --> 00:09:18.131
Oh. well. you won't break me. baby.

195
00:09:18.654 --> 00:09:19.873
Promise?

196
00:09:21.176 --> 00:09:22.379
Oh.

197
00:09:22.847 --> 00:09:24.865
Oh. it's my mom.

198
00:09:24.871 --> 00:09:26.872
Should I send it to voice mail?

199
00:09:26.878 --> 00:09:29.471
Yeah. I'm gonna send that to voicemail.

200
00:09:29.477 --> 00:09:31.239
- Come here.
- Ooh.

201
00:09:31.655 --> 00:09:33.677
- Oh.
- Now I feel guilty.

202
00:09:34.943 --> 00:09:36.749
Maybe I should call her back.

203
00:09:36.755 --> 00:09:39.071
No. I am not calling her back.

204
00:09:39.077 --> 00:09:41.985
- Mm-hmm. Aw.
- No. Maybe just a text.

205
00:09:42.371 --> 00:09:44.469
No. not even a text.

206
00:09:44.665 --> 00:09:48.632
Ugh... I'm sorry. This
can't be a turn on.

207
00:09:48.638 --> 00:09:50.062
You got that right.

208
00:09:50.241 --> 00:09:52.776
You know what? Screw this. Yolanda.

209
00:09:52.782 --> 00:09:54.850
No... screw this Yolanda.

210
00:09:55.641 --> 00:09:58.276
Oh. I intend to.

211
00:09:59.166 --> 00:10:01.181
First I have to tell you something.

212
00:10:01.187 --> 00:10:03.970
Those two months we spent
together were amazing.

213
00:10:04.254 --> 00:10:07.964
I let you go once. I'm not gonna
make the same mistake again.

214
00:10:07.970 --> 00:10:10.065
I'm gonna tell my mother about us.

215
00:10:10.071 --> 00:10:13.331
You would risk your relationship
with your mom for me?

216
00:10:13.337 --> 00:10:14.737
As the saying goes.

217
00:10:14.743 --> 00:10:18.913
"Without pain. without
sacrifice. we would have nothing."

218
00:10:18.983 --> 00:10:21.294
- Old Testament?
- Fight Club.

219
00:10:30.497 --> 00:10:31.965
Hey. Yolanda?!

220
00:10:34.359 --> 00:10:36.359
I'm so glad you're
here. Uh. quick favor...

221
00:10:36.365 --> 00:10:38.085
can you tape me into this box?

222
00:10:39.867 --> 00:10:42.320
Sure. baby. Let me ask you a question.

223
00:10:42.714 --> 00:10:45.578
You think I can impress my
boyfriend's mama in this outfit?

224
00:10:45.777 --> 00:10:47.977
Uh... lose the cleavage.

225
00:10:49.144 --> 00:10:50.344
Mmm.

226
00:10:53.156 --> 00:10:54.356
Uh...

227
00:10:54.640 --> 00:10:56.757
you wanna tell me why I'm doin' this?

228
00:10:56.763 --> 00:10:59.522
I'm tryin' to find out where the
Cash4Crap anonymous warehouse is

229
00:10:59.528 --> 00:11:01.817
so I can save my
relationship with Sofia.

230
00:11:01.823 --> 00:11:04.826
Okay. You do you. girl.

231
00:11:05.474 --> 00:11:07.174
_

232
00:11:19.696 --> 00:11:21.078
You got the stuff?

233
00:11:21.364 --> 00:11:23.558
Oh. I got the stuff. You got the cash?

234
00:11:32.220 --> 00:11:33.822
Do I smell fudge?

235
00:11:34.610 --> 00:11:37.713
100 percent pure brown
gold. First taste is free.

236
00:11:37.719 --> 00:11:39.642
Uh... I... I... I really shouldn't.

237
00:11:39.648 --> 00:11:41.485
- They're fat free.
- Fat free?

238
00:11:43.744 --> 00:11:45.642
Son of a bitch!

239
00:11:45.959 --> 00:11:47.744
Devon! Jenny!

240
00:11:47.750 --> 00:11:50.828
Screw Fat Fighters.
Papa's getting liposuction.

241
00:11:58.774 --> 00:12:02.869
Hey! Gabi! Have you seen my desk chair?

242
00:12:03.339 --> 00:12:04.959
It was here last night.

243
00:12:04.965 --> 00:12:07.456
Now it's gone. and
there's a post-it note

244
00:12:07.462 --> 00:12:09.566
that says. "Thanks. Gabi."

245
00:12:10.099 --> 00:12:13.318
Well... I'm. uh. kinda
sitting in it right now.

246
00:12:13.325 --> 00:12:14.534
Where are you?

247
00:12:14.540 --> 00:12:17.575
Here's the thing... I sold
your chair on Crap4Cash

248
00:12:17.581 --> 00:12:19.603
so I could find out where
the secret warehouse is.

249
00:12:19.609 --> 00:12:22.296
What? Why didn't you sell
something that was yours?

250
00:12:22.302 --> 00:12:24.212
Nobody wants anything I own!

251
00:12:25.301 --> 00:12:27.248
I'm sorry. I... I... I don't
understand what you're doing.

252
00:12:27.254 --> 00:12:30.184
- Are you following the delivery truck?
- Not exactly.

253
00:12:30.190 --> 00:12:32.957
Well. if you're not following the
truck. how do you know where the...

254
00:12:33.082 --> 00:12:34.357
You're sitting in the chair in a box

255
00:12:34.363 --> 00:12:37.066
- in the back of the truck. aren't ya?
- I had to do it. Josh.

256
00:12:37.072 --> 00:12:38.868
It's the only way I could find
out where the secret warehouse

257
00:12:38.874 --> 00:12:41.058
is. and fix my relationship with Sofia.

258
00:12:41.822 --> 00:12:45.268
Shoot. someone's coming! Gotta
go. nothing is impossible. bye!

259
00:12:50.996 --> 00:12:54.235
Hey. guys? Does the secret
warehouse have a bathroom?

260
00:12:54.910 --> 00:12:57.113
Asked the chair...

261
00:12:59.237 --> 00:13:03.353
Okay. I laid out all of
my mom's favorite foods.

262
00:13:03.359 --> 00:13:05.441
Oh. shoot. I forgot dessert.

263
00:13:05.447 --> 00:13:06.971
She loves chocolate.

264
00:13:07.377 --> 00:13:08.746
Let's hope so.

265
00:13:12.876 --> 00:13:14.181
Gabi?

266
00:13:14.187 --> 00:13:15.593
Rabbi Shapiro?

267
00:13:15.981 --> 00:13:18.806
Is your first name... Marilyn?

268
00:13:19.806 --> 00:13:21.901
Gabi. Didn't I just pack you in a box?

269
00:13:21.907 --> 00:13:24.034
Yeah. that's why I'm here.
I... I sold Sofia's couch.

270
00:13:24.040 --> 00:13:26.374
we had a huge fight. and I
tracked it to this apartment.

271
00:13:26.380 --> 00:13:27.700
and now I really gotta get it back.

272
00:13:27.706 --> 00:13:29.474
Oh. girl. I could've
just driven you here.

273
00:13:30.599 --> 00:13:33.213
Now! I know that now!

274
00:13:34.403 --> 00:13:35.883
What are you doing here?

275
00:13:35.889 --> 00:13:38.102
Oh. this is Ben's mother's apartment.

276
00:13:38.108 --> 00:13:40.269
and we're about to
tell her we're dating.

277
00:13:40.275 --> 00:13:42.180
Oh. that's so cute. I didn't
know you guys were tog...

278
00:13:42.452 --> 00:13:44.467
Oh. my God. my couch!

279
00:13:44.757 --> 00:13:46.584
She's here!

280
00:13:47.085 --> 00:13:48.392
- What's. uh. what's...
- Benny!

281
00:13:48.398 --> 00:13:51.091
- what's happening?
- Your girlfriend is gorgeous!

282
00:13:51.097 --> 00:13:52.592
- No... Ma... no.
- No... oh...

283
00:13:52.598 --> 00:13:54.600
- uh. Mrs. Shapiro. I...
- A little young...

284
00:13:54.606 --> 00:13:56.535
- but gorgeous.
- Oh. uh. Mrs. Shapiro. I'm

285
00:13:56.541 --> 00:13:58.833
- actually... I'm here to...
- And look at that tuchus.

286
00:13:59.838 --> 00:14:03.658
Sometimes God gives with two hands.

287
00:14:03.664 --> 00:14:05.853
- But... Ma...
- Mrs. Shapiro. really. I... I'm...

288
00:14:05.859 --> 00:14:07.832
I'm actually just here
because I really want...

289
00:14:07.838 --> 00:14:10.704
Sweetie you give me a grandchild.

290
00:14:10.710 --> 00:14:13.517
you can have anything you want.

291
00:14:16.149 --> 00:14:19.172
- Can I have your couch? Oh!
- Anything!

292
00:14:19.178 --> 00:14:21.049
Benny! We're goin' off the pill!

293
00:14:22.048 --> 00:14:25.127
You've made me the
happiest woman on Earth!

294
00:14:25.510 --> 00:14:28.590
Are you gonna tell your mother
I'm your girlfriend or not?

295
00:14:28.596 --> 00:14:30.533
I've never seen her so happy!

296
00:14:30.931 --> 00:14:33.730
- Are you kidding me?
- All right. I'm sorry. I'm sorry.

297
00:14:33.736 --> 00:14:36.322
This time it's gonna
require a little Schnapps.

298
00:14:36.328 --> 00:14:37.698
Well... I thought you
said she couldn't drink

299
00:14:37.704 --> 00:14:39.587
- because of her high blood pressure?
- For me!

300
00:14:39.593 --> 00:14:41.180
- Oh.
- It's for me!

301
00:14:41.673 --> 00:14:43.904
I'm so happy to see you.

302
00:14:43.910 --> 00:14:45.984
Oh. you think you're happy.

303
00:14:45.990 --> 00:14:48.793
Benny never brought home a girl before.

304
00:14:48.799 --> 00:14:52.184
And so blonde and beautiful.

305
00:14:52.697 --> 00:14:54.898
I'd never know you're Jewish.

306
00:14:57.792 --> 00:15:01.025
You are... Jewish. right?

307
00:15:02.085 --> 00:15:05.659
Yes! Yes. I'm... I'm very
Jewish. I'm like. uh...

308
00:15:05.665 --> 00:15:10.112
like. uh. Hanukkah. um. uh.
Seth Rogen. deli platters!

309
00:15:12.087 --> 00:15:13.974
Ma. I got somethin' to tell ya.

310
00:15:13.980 --> 00:15:17.560
- That girl that you're talking to...
- Is an angel!

311
00:15:17.566 --> 00:15:19.058
She's not my girlfriend.

312
00:15:19.064 --> 00:15:21.899
- What?
- This is my girlfriend.

313
00:15:23.280 --> 00:15:25.375
Then who the hell's this?!

314
00:15:25.381 --> 00:15:28.372
Um... I'm just a...
poor little Jewish girl

315
00:15:28.378 --> 00:15:30.569
who really. really wants your couch.

316
00:15:31.298 --> 00:15:34.612
Oy! I'm... so sorry
for the mix-up. Yolanda.

317
00:15:38.469 --> 00:15:42.866
Well... I guess you're
more age-appropriate.

318
00:15:43.380 --> 00:15:46.529
Benny. if... if you're
happy. then I'm happy.

319
00:15:46.622 --> 00:15:48.999
Ma. I've never been happier.

320
00:15:49.005 --> 00:15:52.906
Oh. it's a pleasure
to meet you. Yo-lan-da?

321
00:15:52.912 --> 00:15:56.620
Oh... oh... the pleasure's mine.

322
00:15:56.626 --> 00:16:00.526
I never met an African-American Jew.

323
00:16:01.790 --> 00:16:05.395
- Mmm...
- Are you from the lost tribe of...?

324
00:16:05.401 --> 00:16:07.187
Ethiopia!

325
00:16:08.121 --> 00:16:10.182
Uh... t... uh... t...

326
00:16:10.730 --> 00:16:12.636
uh... uh...

327
00:16:12.839 --> 00:16:16.355
More like the lost tribe of Inglewood.

328
00:16:16.644 --> 00:16:17.844
Uh...

329
00:16:19.855 --> 00:16:22.959
I'm. uh... I'm not Jewish.

330
00:16:22.965 --> 00:16:25.810
- What?
- No. Ma... it doesn't matter

331
00:16:25.816 --> 00:16:28.254
- if she's Jewish or not. I love her.
- Ah. ahh...

332
00:16:28.260 --> 00:16:31.161
- Ma. Ma! Oh. my... Ma... Let's...
- Mrs. Shapiro?!

333
00:16:31.167 --> 00:16:32.678
- Let's bring... the Dr. Schwartz
- I can't

334
00:16:32.684 --> 00:16:34.851
- next door! All right. come on. Ma.
- breathe! I can't

335
00:16:34.857 --> 00:16:37.764
- Oh. for the love of Jesus!
- breathe! Oh. don't say Jesus!

336
00:16:37.770 --> 00:16:41.183
- All right... let's go.
- I'm going fast!

337
00:16:41.189 --> 00:16:43.493
Slow down. Ma. Just don't worry.

338
00:16:44.084 --> 00:16:46.207
Oh. my God.

339
00:16:49.376 --> 00:16:51.152
Hello. old friend.

340
00:16:51.789 --> 00:16:53.399
It's a mitzvah!

341
00:16:57.863 --> 00:17:00.944
Don't think I brought you
back 'cause I like you. okay?

342
00:17:01.842 --> 00:17:04.076
'Cause I don't... I did it for Sofia.

343
00:17:04.404 --> 00:17:06.329
Oh. my God. she's here. She's here!

344
00:17:06.522 --> 00:17:08.625
So-fi-a...

345
00:17:08.844 --> 00:17:10.044
Hi.

346
00:17:10.220 --> 00:17:12.027
Okay. you're not gonna
be mad at me in a minute.

347
00:17:12.033 --> 00:17:13.829
because I have a huge surprise for you.

348
00:17:13.835 --> 00:17:15.303
Yeah. well. I kinda
have one for you. too.

349
00:17:15.309 --> 00:17:17.598
Okay. mine's more of a
visual. so let's go inside...

350
00:17:17.604 --> 00:17:20.117
No. um... tryin' to
be a better listener.

351
00:17:20.123 --> 00:17:23.344
so. um... you go first.

352
00:17:24.363 --> 00:17:27.268
Well. Logan's not coming
over to see the couch.

353
00:17:31.767 --> 00:17:33.003
What?

354
00:17:33.674 --> 00:17:36.032
Yeah. it turned out
that giving me that couch

355
00:17:36.038 --> 00:17:39.014
was just a mean joke she and her
rich friends play on their assistants.

356
00:17:39.020 --> 00:17:41.646
Pfft. So... what's your surprise?

357
00:17:53.870 --> 00:17:55.382
Ohh.

358
00:17:55.501 --> 00:17:58.892
Oh... well... so I guess it wasn't
that hard to get back after all.

359
00:18:02.095 --> 00:18:04.409
Nope. No... not at all.

360
00:18:04.415 --> 00:18:06.928
I just had to... pack myself into a box

361
00:18:06.934 --> 00:18:08.756
to get to the warehouse. so
I could steal the invoice.

362
00:18:08.762 --> 00:18:10.325
to get the address of the
person who bought the couch.

363
00:18:10.331 --> 00:18:11.934
which turned out to be
Yolanda's boyfriend's mother.

364
00:18:11.940 --> 00:18:13.536
who would only give me
the couch if I was Jewish.

365
00:18:13.542 --> 00:18:15.521
and willing to make babies with her son.

366
00:18:17.243 --> 00:18:19.929
So I told her I was Jewish. and
then she fainted. which gave me

367
00:18:19.935 --> 00:18:21.952
the opportunity to steal the
couch back for my best friend.

368
00:18:21.958 --> 00:18:24.014
who I love more than
anything else in the world.

369
00:18:27.226 --> 00:18:30.813
So. uh... you know... now's where
you hug me. and you say you love me.

370
00:18:30.819 --> 00:18:33.410
and. uh. it's all. you know.
water under the couch...

371
00:18:34.267 --> 00:18:36.196
Yeah. well... it's not.

372
00:18:36.202 --> 00:18:37.659
It's not? Why is it not?

373
00:18:37.963 --> 00:18:39.562
Because that's not the problem.

374
00:18:39.568 --> 00:18:42.135
The problem is that you don't respect
me. and you don't listen to me.

375
00:18:42.141 --> 00:18:44.896
and you don't care about how
important my career is to me.

376
00:18:44.902 --> 00:18:48.936
Oh... well. uh. guess. uh. just. uh.
nothin'. uh. nothin' more to say. then.

377
00:18:49.421 --> 00:18:50.632
What's that?

378
00:18:50.638 --> 00:18:52.148
Nothing. But open it.

379
00:18:56.117 --> 00:18:57.406
"Intro to Journalism"?

380
00:18:57.412 --> 00:19:00.486
Huh. I... I took the $800
I made selling the couch

381
00:19:00.492 --> 00:19:02.317
and I signed you up for night classes.

382
00:19:02.603 --> 00:19:03.803
What?

383
00:19:04.081 --> 00:19:07.583
Yeah. because you're a
writer! See. I listen!

384
00:19:09.151 --> 00:19:11.499
And if... and if you really.
really wanna be a journalist.

385
00:19:11.505 --> 00:19:13.484
then you need some training
to go with that natural talent

386
00:19:13.490 --> 00:19:14.772
that you already have.

387
00:19:14.778 --> 00:19:17.594
Oh. Gabi. it... I
don't know what to say.

388
00:19:17.669 --> 00:19:19.507
That's why I got you the class!

389
00:19:23.818 --> 00:19:26.241
And... you know what I realized?

390
00:19:26.247 --> 00:19:28.250
I mean. this whole time. I wasn't
trying to get the couch back.

391
00:19:28.255 --> 00:19:30.621
I was trying to get you
back. you know? Because...

392
00:19:30.627 --> 00:19:33.476
because... you're my ugly couch.

393
00:19:36.623 --> 00:19:40.588
- And you're my ugly couch!
- Ohhh!

394
00:19:40.870 --> 00:19:42.079
Owww!

395
00:19:42.085 --> 00:19:45.509
Jeez. Oy. this fakakta couch already.

396
00:19:47.322 --> 00:19:50.095
So. we waited at the hospital for hours.

397
00:19:50.101 --> 00:19:53.977
and when she finally came to.
she forbid Ben from seein' me.

398
00:19:54.089 --> 00:19:57.831
But. the good news is. right
after that. she fell into a coma.

399
00:19:59.104 --> 00:20:00.504
That's horrible.

400
00:20:00.510 --> 00:20:02.703
Not for my sex life.

401
00:20:02.836 --> 00:20:05.808
As long as she's out... the son is in!

402
00:20:09.222 --> 00:20:10.756
What you need. bro?

403
00:20:11.565 --> 00:20:14.055
I need a fix... got a dime bag?

404
00:20:15.940 --> 00:20:18.367
Careful with that... it's uncut.

405
00:20:21.002 --> 00:20:23.020
I gained six pounds?

406
00:20:23.026 --> 00:20:25.429
So... all of us gained weight.

407
00:20:25.435 --> 00:20:26.703
How is that even possible?

408
00:20:26.709 --> 00:20:29.601
We've been exercising.
counting points...

409
00:20:29.607 --> 00:20:31.602
the only outside food
that I've eaten is...

410
00:20:33.381 --> 00:20:36.102
Oh. my God!

411
00:20:36.486 --> 00:20:39.078
Whoa. whoa. whoa. hold up!

412
00:20:39.265 --> 00:20:41.096
Before you do anything...

413
00:20:41.102 --> 00:20:42.992
consider this...

414
00:20:43.992 --> 00:20:46.273
is anyone really gonna notice
that you gained six pounds?

