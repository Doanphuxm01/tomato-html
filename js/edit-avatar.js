$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){
    // change avatar
    $(document).off('click', 'div#change-profile-pic');
    $(document).on('click', 'div#change-profile-pic', (function(){
        LoadCss('/css/imgareaselect.css');
        LoadScript('/js/jquery.imgareaselect.js');
        LoadScript('/js/jquery.form.js');

        $('#profile_pic_modal').modal({show:true});
        $('#profile-pic').val(''); 
        $("#photo").attr("src","");
        $('#hdn-x1-axis').val('');
        $('#hdn-y1-axis').val('');
        $('#hdn-x2-axis').val('');
        $('#hdn-y2-axis').val('');
        $('#hdn-thumb-width').val('');
        $('#hdn-thumb-height').val('');

    }));

    $('#profile-pic').on('change', function() {
        
        $("#preview-profile-pic").html('');
        $("#preview-profile-pic").html('Đang tải lên...');
        $("#cropimage").ajaxForm(
        {
            target: '#preview-profile-pic',
            success: function() {
                $('img#photo').imgAreaSelect({
                    aspectRatio: '1:1',
                    onSelectEnd: getSizes,
                });
                $('#image_name').val($('#photo').attr('file-name'));
            }
        }).submit();
        
    });

	/* handle functionality when click crop button  */
	$('#save_crop').on('click', function(e){
        e.preventDefault();
        if($('#profile-pic').val()==''){
            $('div#preview-profile-pic').html("<span style='padding-left:1em; color:crimson;'>Vui lòng chọn ảnh...!</span>");
            return false;
        }

        params = {
            targetUrl: '/change_pic?action=save',
            action: 'save',
            x_axis: $('#hdn-x1-axis').val(),
            y_axis : $('#hdn-y1-axis').val(),
            x2_axis: $('#hdn-x2-axis').val(),
            y2_axis : $('#hdn-y2-axis').val(),
            thumb_width : $('#hdn-thumb-width').val(),
            thumb_height:$('#hdn-thumb-height').val()
        };
        saveCropImage(params);
    });
    /* Function to get images size */
    function getSizes(img, obj){
        var x_axis = obj.x1;
        var x2_axis = obj.x2;
        var y_axis = obj.y1;
        var y2_axis = obj.y2;
        var thumb_width = obj.width;
        var thumb_height = obj.height;
        if(thumb_width > 0) {
			$('#hdn-x1-axis').val(x_axis);
			$('#hdn-y1-axis').val(y_axis);
			$('#hdn-x2-axis').val(x2_axis);
			$('#hdn-y2-axis').val(y2_axis);
			$('#hdn-thumb-width').val(thumb_width);
			$('#hdn-thumb-height').val(thumb_height);
        } else {
            alert("Ấn và giữ kéo chuột để cắt ảnh...");
		}
    }
    /* Function to save crop images */
    function saveCropImage(params) {
        params['thumb_width']==''?w1=$('img#photo').width():w1=params['thumb_width'];
        params['thumb_height']==''?h1=$('img#photo').height():h1=params['thumb_height'];
        params['x_axis']==''?x1=0:x1=params['x_axis'];
        params['y_axis']==''?y1=0:y1=params['y_axis'];

		jQuery.ajax({
			url: params['targetUrl'],
			cache: false,
			dataType: "html",
			data: {
				action: params['action'],
				id: $('#hdn-profile-id').val(),
                t: 'ajax',
                w1:w1,
                x1:x1,
                h1:h1,
                y1:y1,
                x2:params['x2_axis'],
                y2:params['y2_axis'],
                image_name :$('#image_name').val()
			},
			type: 'Post',
		   	success: function (response) {
                if(response == 'error_1'){
                    $('div#preview-profile-pic').html("<span style='padding-left:1em; color:crimson;'>Vui lòng chọn ảnh!!!</span>");
                    return false;
                }
                $('#profile_pic_modal').modal('hide');
                $(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4,.imgareaselect-border2,.imgareaselect-outer").css('display', 'none');
                
                $("#profile_picture").attr('src', response);
                $("#preview-profile-pic").html('');
                $("#profile-pic").val();

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert('status Code:' + xhr.status + 'Error Message :' + thrownError);
			}
		});
    }

    function LoadCss(url) {
        var link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = url;
        document.getElementsByTagName("head")[0].appendChild(link);
    }
    function LoadScript(url) {
        var script = document.createElement('script');
        script.setAttribute('src', url);
        script.setAttribute('async', false);
        document.head.appendChild(script);
    }

    function guest(){
        ms = 'Bạn phải đăng nhập mới sử dụng được chức năng này !';
        alertP('crimson',ms,300);
    }

});
